# Vending machine

## Running the app
Clone this repository, cd into its directory and run:

```bash
yarn
yarn start
```

`npm install` and `npm run start` should work fine too, however the lockfile has been generated with yarn.

On the home page is the vending machine interface that allows you to select an item and specify the amount of money you're paying with. A modal will pop up showing the item you bought and the change allocated by the machine. 

You can log in via the button at the top right, the login information is already filled out in the form - it's just a spoofed login with a setTimeout to emulate a request, and refreshing the page will require you to log in again. 

Once logged in, you can access the manage page via the button in the navbar, from there you can see the list of coins in the machine, and you can change those inputs inline to update the number of each coin type in the machine. Below that is the list of products, each with a name, cost and number. You can change these and click on save to update an item, delete to remove it and at the bottom of the list you can fill out information for a new item and add it. 
