import { useAppSelector } from "../hooks";
import ButtonLink from "./ButtonLink";
import LoadingSpinner from "./LoadingSpinner";

export default function Navbar() {
  const authenticated = useAppSelector((state) => state.auth.authenticated);
  const loggingIn = useAppSelector((state) => state.auth.loading);

  return (
    <div className="h-20 px-4 text-white p-1 bg-pink-900">
      <div className="container mx-auto flex flex-row justify-end items-center h-full">
        <ButtonLink className="mr-1" to="/">
          Home
        </ButtonLink>
        {authenticated && <ButtonLink to="/Manage">Manage Machine</ButtonLink>}
        {!authenticated && !loggingIn && (
          <ButtonLink to="/login">Login</ButtonLink>
        )}
        {!authenticated && loggingIn && <LoadingSpinner />}
      </div>
    </div>
  );
}
