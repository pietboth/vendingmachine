import Button from "./Button";

type Props = {
  visible: boolean;
  header: string;
  children: JSX.Element | string;
  confirmText?: string;
  onDismiss: () => void;
};

export default function Modal({
  visible,
  header,
  children,
  onDismiss,
  confirmText = "Thanks!",
}: Props) {
  if (!visible) {
    return null;
  }

  return (
    <div className="h-screen absolute w-full flex flex-col items-center justify-center bg-teal-lightest font-sans top-0 left-0">
      <div className="z-0 w-full h-full absolute top-0 left-0 bg-black bg-opacity-75"></div>
      <div className="z-10 h-screen w-full flex items-center justify-center bg-modal">
        <div className="bg-white rounded shadow p-8 m-4 max-w-xs max-h-full text-center overflow-y-auto">
          <div className="mb-4">
            <h1>{header}</h1>
          </div>
          <div className="mb-8">{children}</div>
          <div className="flex justify-center">
            <Button
              onClick={onDismiss}
              className="flex-no-shrink text-white py-2 px-4 rounded bg-teal hover:bg-teal-dark"
            >
              {confirmText}
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
