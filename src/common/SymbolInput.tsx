import cx from "classnames";

type Props = React.InputHTMLAttributes<HTMLInputElement> & {
  symbol?: string;
};

export default function SymbolInput({
  symbol = "£",
  className,
  ...props
}: Props) {
  return (
    <div className={cx("pl-6 relative bg-white flex w-min", className)}>
      <span className="absolute bottom-0 left-0 top-0 inputSymbol bg-gray-200">
        {symbol}
      </span>
      <input type="text" className="input" {...props} />
    </div>
  );
}
