import { Link } from "react-router-dom";
import cx from "classnames";

type Props = {
  to: string;
  children: JSX.Element | string;
  className?: string;
};
export default function ButtonLink({ to, children, className }: Props) {
  return (
    <Link
      to={to}
      className={cx(
        "focus:outline-none text-white text-sm py-2.5 px-5 rounded-md bg-blue-500 hover:bg-blue-600 hover:shadow-lg",
        className
      )}
    >
      {children}
    </Link>
  );
}
