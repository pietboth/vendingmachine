import cx from "classnames";
import React from "react";

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  children: JSX.Element | string;
  className?: string;
};
export default function Button({
  children,
  className,
  disabled,
  onClick,
  ...props
}: Props) {
  const onClicked = (e: React.MouseEvent<HTMLButtonElement>) => {
    if (disabled) {
      e.preventDefault();
      e.stopPropagation();
    } else if (onClick) {
      onClick(e);
    }
  };

  return (
    <button
      className={cx(
        "focus:outline-none text-white text-sm py-2.5 px-5 rounded-md ",
        {
          "bg-blue-500 hover:bg-blue-600 hover:shadow-lg": !disabled,
          "bg-blue-200 hover:bg-blue-200": disabled,
        },
        className
      )}
      onClick={onClicked} //!disabled ? onClick : undefined}
      {...props}
    >
      {children}
    </button>
  );
}
