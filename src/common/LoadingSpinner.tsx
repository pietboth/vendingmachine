import styles from "./LoadingSpinner.module.css";

export default function LoadingSpinner() {
  return (
    <div className={styles.ldsEllipsis}>
      <div className="bg-purple-600"></div>
      <div className="bg-purple-600"></div>
      <div className="bg-purple-600"></div>
      <div className="bg-purple-600"></div>
    </div>
  );
}
