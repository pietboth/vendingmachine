import LoadingSpinner from "./LoadingSpinner";

export default function PageLoading() {
  return (
    <div className="container mx-auto flex items-center justify-center">
      <LoadingSpinner />
    </div>
  );
}
