import cx from "classnames";

type Props = React.ButtonHTMLAttributes<HTMLButtonElement> & {
  children: JSX.Element | string;
  className?: string;
};
export default function DeleteButton({
  children,
  className,
  disabled,
  ...props
}: Props) {
  return (
    <button
      className={cx(
        "focus:outline-none text-white text-sm py-2.5 px-5 rounded-md ",
        {
          "bg-red-500 hover:bg-red-600 hover:shadow-lg": !disabled,
          "bg-red-200 hover:bg-red-200": disabled,
        },
        className
      )}
      type="button"
      {...props}
    >
      {children}
    </button>
  );
}
