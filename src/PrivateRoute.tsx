import { Redirect, Route, RouteProps } from "react-router";
import { useAppSelector } from "./hooks";

type Props = RouteProps;

export default function PrivateRoute({ children, ...rest }: Props) {
  const authenticated = useAppSelector((state) => state.auth.authenticated);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        authenticated ? (
          children
        ) : (
          <Redirect to={{ pathname: "/login", state: { from: location } }} />
        )
      }
    />
  );
}
