import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../hooks";
import ProductListItem from "./ProductListItem";
import { Product } from "./types";
import { setSelectedProduct } from "./vendingMachineSlice";

export default function ProductList() {
  const products = useAppSelector((state) => state.vendingMachine.products);
  const selectedProduct = useAppSelector(
    (state) => state.vendingMachine.selectedProduct
  );

  const dispatch = useAppDispatch();

  const onSelect = useCallback(
    (product: Product) => {
      if (product.count > 0) {
        // toggle product selection
        dispatch(
          setSelectedProduct(selectedProduct === product ? null : product)
        );
      }
    },
    [dispatch, selectedProduct]
  );

  return (
    <div className="grid md:grid-cols-3 xl:grid-cols-4 grid-cols-1 auto-rows-auto gap-1">
      {products.map((p) => (
        <ProductListItem
          key={p.name}
          product={p}
          onSelect={onSelect}
          selected={selectedProduct === p}
        />
      ))}
    </div>
  );
}
