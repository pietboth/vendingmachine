export type ChangeCurrencyItem = {
  currencyValue: number;
  count: number;
};

export type ChangeCalculationResult = {
  haveChange: boolean;
  totalChange: number;
  changeItems: ChangeCurrencyItem[];
};

export type Product = {
  id: number;
  name: string;
  cost: number;
  count: number;
};

export type AddProductRequest = {
  name: string;
  cost: number;
  count: number;
};

export type BuyRequest = {
  product: Product;
  amountPaid: number;
};

export type PayResult = {
  error: string | null;
  currencyAfterPayment: ChangeCurrencyItem[];
  totalChange: number;
};

export interface ChangeDueResult {
  total: number;
  changeCurrency: ChangeCurrencyItem[];
}
