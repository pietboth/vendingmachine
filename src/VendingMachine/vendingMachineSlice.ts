import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { pay, reduceProductStock } from "./payment";
import {
  AddProductRequest,
  BuyRequest,
  ChangeCurrencyItem,
  ChangeDueResult,
  Product,
} from "./types";

interface VendingMachineState {
  changeCoins: ChangeCurrencyItem[];
  products: Product[];
  selectedProduct: Product | null;
  purchasedProduct: Product | null;
  changeDue: ChangeDueResult | null;
  paymentError: string | null;
}

const initialState: VendingMachineState = {
  changeCoins: [
    { currencyValue: 200, count: 3 },
    { currencyValue: 100, count: 5 },
    { currencyValue: 50, count: 10 },
    { currencyValue: 20, count: 3 },
    { currencyValue: 10, count: 5 },
    { currencyValue: 5, count: 5 },
    { currencyValue: 2, count: 5 },
    { currencyValue: 1, count: 100 },
  ],
  products: [
    { id: 1, name: "Coca-Cola", cost: 120, count: 5 },
    { id: 2, name: "Skittles", cost: 200, count: 2 },
    { id: 3, name: "Snickers", cost: 85, count: 10 },
    { id: 4, name: "Moams", cost: 169, count: 3 },
    { id: 5, name: "Mars Bar", cost: 88, count: 0 },
  ],
  selectedProduct: null,
  changeDue: null,
  paymentError: null,
  purchasedProduct: null,
};

export const vendingMachineSlice = createSlice({
  name: "vendingMachine",
  initialState,
  reducers: {
    setChangeCoins: (
      state: VendingMachineState,
      action: PayloadAction<ChangeCurrencyItem[]>
    ) => {
      state.changeCoins = action.payload;
    },

    setSelectedProduct: (
      state: VendingMachineState,
      action: PayloadAction<Product | null>
    ) => {
      state.selectedProduct = action.payload;
    },

    buyProduct: (
      state: VendingMachineState,
      action: PayloadAction<BuyRequest>
    ) => {
      const { error, currencyAfterPayment, totalChange, changeItems } = pay(
        action.payload.product,
        action.payload.amountPaid,
        state.changeCoins
      );

      if (error) {
        state.paymentError = error;
        state.changeDue = null;
      } else {
        state.changeDue = { total: totalChange, changeCurrency: changeItems };
        state.changeCoins = currencyAfterPayment;
        state.paymentError = null;
        state.selectedProduct = null;
        state.purchasedProduct = action.payload.product;
        state.products = reduceProductStock(
          state.products,
          action.payload.product,
          1
        );
      }
    },

    confirmPurchaseInformation: (state) => {
      state.changeDue = null;
      state.paymentError = null;
      state.purchasedProduct = null;
    },

    changeCoinAmount: (state, action: PayloadAction<ChangeCurrencyItem>) => {
      state.changeCoins = state.changeCoins.map((c) =>
        c.currencyValue === action.payload.currencyValue ? action.payload : c
      );
    },

    updateProduct: (state, action: PayloadAction<Product>) => {
      state.products = state.products.map((p) =>
        p.id === action.payload.id ? action.payload : p
      );
    },

    addProduct: (state, action: PayloadAction<AddProductRequest>) => {
      const id =
        state.products.length > 0
          ? state.products[state.products.length - 1].id + 1
          : 1;
      state.products.push({ id, ...action.payload });
    },

    deleteProduct: (state, action: PayloadAction<number>) => {
      state.products = state.products.filter((p) => p.id !== action.payload);
    },
  },
});

export const {
  setChangeCoins,
  setSelectedProduct,
  buyProduct,
  confirmPurchaseInformation,
  changeCoinAmount,
  updateProduct,
  addProduct,
  deleteProduct,
} = vendingMachineSlice.actions;

export default vendingMachineSlice.reducer;
