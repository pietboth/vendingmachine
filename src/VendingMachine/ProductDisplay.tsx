import { Product } from "./types";
import formatDisplayPrice from "./formatDisplayPrice";

type Props = {
  product: Product;
  showRemaining: boolean;
};

export default function ProductDisplay({ product, showRemaining }: Props) {
  return (
    <div>
      <p>{product.name}</p>
      <p>
        {formatDisplayPrice(product.cost)}
        {showRemaining && <>- {product.count} left</>}
      </p>
    </div>
  );
}
