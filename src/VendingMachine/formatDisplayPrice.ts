export default function formatDisplayPrice(amountInPence: number) {
  const pounds = Math.trunc(amountInPence / 100);
  const pence = Math.round(amountInPence - pounds * 100);

  return `£${pounds}.${pence.toString().padStart(2, "0")}`;
}
