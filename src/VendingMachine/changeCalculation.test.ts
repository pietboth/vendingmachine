import { calculateChange, getChangeInCoins } from "./changeCalculation";
import { ChangeCurrencyItem } from "./types";

describe("Change calculation", () => {
  describe("Calculating change", () => {
    it("should return positive amount if change is required", () => {
      const result = calculateChange(23.5, 25.0);

      expect(result).toBe(1.5);
    });
  });

  describe("Get change in coins", () => {
    let coins: ChangeCurrencyItem[] = [];

    beforeEach(() => {
      coins = [
        { currencyValue: 200, count: 3 },
        { currencyValue: 100, count: 5 },
        { currencyValue: 50, count: 10 },
        { currencyValue: 20, count: 3 },
        { currencyValue: 10, count: 5 },
        { currencyValue: 5, count: 5 },
        { currencyValue: 2, count: 5 },
        { currencyValue: 1, count: 100 },
      ];
    });

    it("should calculate the correct amount from only £2 coins", () => {
      const { haveChange, totalChange, changeItems } = getChangeInCoins(
        400,
        coins
      );
      expect(haveChange).toBe(true);
      expect(totalChange).toBe(400);
      expect(changeItems.length).toBe(1);
      expect(changeItems[0].currencyValue).toBe(200);
      expect(changeItems[0].count).toBe(2);
    });

    it("should calculate the correct amount from only £1 and 20p coins", () => {
      const { haveChange, totalChange, changeItems } = getChangeInCoins(
        120,
        coins
      );
      expect(haveChange).toBe(true);
      expect(totalChange).toBe(120);
      expect(changeItems.length).toBe(2);
      expect(changeItems[0].currencyValue).toBe(100);
      expect(changeItems[0].count).toBe(1);
      expect(changeItems[1].currencyValue).toBe(20);
      expect(changeItems[1].count).toBe(1);
    });

    it("should calculate no change if change required is greater than amount available", () => {
      const { haveChange, totalChange, changeItems } = getChangeInCoins(
        200000,
        coins
      );
      expect(haveChange).toBe(false);
      expect(totalChange).toBe(0);
      expect(changeItems.length).toBe(0);
    });

    it("should calculate no change if change required cannot be returned from available coins", () => {
      // no 1p coin
      const { haveChange, totalChange, changeItems } = getChangeInCoins(126, [
        { currencyValue: 100, count: 2 },
        { currencyValue: 10, count: 2 },
        { currencyValue: 5, count: 1 },
      ]);
      expect(haveChange).toBe(false);
      expect(totalChange).toBe(0);
      expect(changeItems.length).toBe(0);
    });

    it("should calculate the correct amount from only £2, 50p, 10p and 5p coins", () => {
      const { haveChange, totalChange, changeItems } = getChangeInCoins(
        265,
        coins
      );
      expect(haveChange).toBe(true);
      expect(totalChange).toBe(265);
      expect(changeItems.length).toBe(4);
      expect(changeItems[0].currencyValue).toBe(200);
      expect(changeItems[0].count).toBe(1);
      expect(changeItems[1].currencyValue).toBe(50);
      expect(changeItems[1].count).toBe(1);
      expect(changeItems[2].currencyValue).toBe(10);
      expect(changeItems[2].count).toBe(1);
      expect(changeItems[3].currencyValue).toBe(5);
      expect(changeItems[3].count).toBe(1);
    });
  });
});
