import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../hooks";
import ChangeDisplay from "./ChangeDisplay";
import ProductListEdit from "./ProductListEdit";
import { ChangeCurrencyItem } from "./types";
import { changeCoinAmount } from "./vendingMachineSlice";

export default function ManageMachine() {
  const coins = useAppSelector((state) => state.vendingMachine.changeCoins);
  const dispatch = useAppDispatch();

  const onCoinAmountChanged = useCallback(
    (coin: ChangeCurrencyItem) => {
      dispatch(changeCoinAmount(coin));
    },
    [dispatch]
  );

  return (
    <div className="">
      <h1 className="text-center md:text-left mt-2 mb-5">
        Coins currently in vending machine (change values to edit):
      </h1>
      <ChangeDisplay
        coins={coins}
        editable={true}
        onCoinAmountChanged={onCoinAmountChanged}
      />
      <h1 className="mt-10 mb-1 text-center md:text-left">Products</h1>

      <div className="mt-2">
        <ProductListEdit />
      </div>
    </div>
  );
}
