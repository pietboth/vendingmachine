import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "../hooks";
import ProductEditItem from "./ProductEditItem";
import { Product } from "./types";
import {
  addProduct,
  deleteProduct,
  updateProduct,
} from "./vendingMachineSlice";

export default function ProductListEdit() {
  const dispatch = useAppDispatch();
  const products = useAppSelector((state) => state.vendingMachine.products);
  const newProduct = {
    id: 0,
    name: "",
    cost: 0,
    count: 0,
  };

  const onProductChanged = useCallback(
    (product: Product) => {
      dispatch(updateProduct(product));
    },
    [dispatch]
  );

  const onProductDeleted = useCallback(
    (product: Product) => {
      dispatch(deleteProduct(product.id));
    },
    [dispatch]
  );

  const onProductAdded = useCallback(
    (product: Product) => {
      dispatch(addProduct(product));
    },
    [dispatch]
  );

  return (
    <div className="flex flex-col items-center md:items-start justify-start">
      {products.map((p, i) => (
        <ProductEditItem
          key={`${p.name}_${i}`}
          product={p}
          onChange={onProductChanged}
          onDelete={onProductDeleted}
        />
      ))}

      <ProductEditItem
        product={newProduct}
        onChange={onProductAdded}
        saveText="Add Product"
      />
    </div>
  );
}
