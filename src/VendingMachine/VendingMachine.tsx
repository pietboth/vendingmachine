import { useAppSelector } from "../hooks";
import Coinslot from "./Coinslot";
import ProductList from "./ProductList";
import PurchaseConfirmationModal from "./PurchaseConfirmationModal";

export default function VendingMachine() {
  const paymentError = useAppSelector(
    (state) => state.vendingMachine.paymentError
  );

  return (
    <div className="flex flex-col">
      <div className="mt-1 flex md:flex-row flex-col items-start justify-start">
        <div className="mr-1 w-full md:w-max px-4">
          <ProductList />
        </div>

        <div className="w-full md:w-1/3">
          <Coinslot />
        </div>
      </div>

      <PurchaseConfirmationModal />

      {paymentError && (
        <div className="mt-1">
          <p className="text-pink-600">{paymentError}</p>
        </div>
      )}
    </div>
  );
}
