import { calculateChange, getChangeInCoins } from "./changeCalculation";
import { ChangeCurrencyItem, Product } from "./types";

export function pay(
  item: Product,
  amountPaid: number,
  currentCurrencyItems: ChangeCurrencyItem[]
) {
  if (item.count <= 0) {
    // this shouldn't happen
    return {
      error: "That item is out of stock.",
      currencyAfterPayment: currentCurrencyItems,
      totalChange: 0,
      changeItems: [],
    };
  }

  if (item.cost > amountPaid) {
    return {
      error: "Not enough money provided, please insert more coins",
      currencyAfterPayment: currentCurrencyItems,
      totalChange: 0,
      changeItems: [],
    };
  }

  const changeRequired = calculateChange(item.cost, amountPaid);
  const { haveChange, totalChange, changeItems } = getChangeInCoins(
    changeRequired,
    currentCurrencyItems
  );

  if (changeRequired && !haveChange) {
    return {
      error:
        "The vending machine is not able to produce change for this item and the amount you paid.",
      currencyAfterPayment: currentCurrencyItems,
      totalChange: 0,
      changeItems: [],
    };
  }

  const newCurrency = currentCurrencyItems.map((current) => {
    const found = changeItems.find(
      (change) => change.currencyValue === current.currencyValue
    );

    return found ? { ...current, count: current.count - found.count } : current;
  });

  return {
    error: null,
    currencyAfterPayment: newCurrency,
    totalChange,
    changeItems,
  };
}

export function reduceProductStock(
  products: Product[],
  product: Product,
  count: number
) {
  return products.map((p) =>
    p.name !== product.name ? p : { ...p, count: p.count - count }
  );
}
