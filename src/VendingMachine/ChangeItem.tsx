import React, { useCallback } from "react";
import { ChangeCurrencyItem } from "./types";

function coinDisplayValue(coin: ChangeCurrencyItem) {
  const pence = coin.currencyValue < 100;
  return `${pence ? "" : "£"}${
    pence ? coin.currencyValue : coin.currencyValue / 100
  }${pence ? "p" : ""}`;
}

type Props = {
  coin: ChangeCurrencyItem;
  editableAmounts?: boolean;
  onCoinAmountChanged?: (coin: ChangeCurrencyItem) => void;
};

export default function ChangeItem({
  coin,
  editableAmounts = false,
  onCoinAmountChanged,
}: Props) {
  const onEdit = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      if (onCoinAmountChanged) {
        const newCount = parseInt(e.target.value);
        onCoinAmountChanged({
          ...coin,
          count: !isNaN(newCount) ? newCount : coin.count,
        });
      }
    },
    [coin, onCoinAmountChanged]
  );

  return (
    <div className="flex flex-col items-center justify-center">
      <div
        key={coin.currencyValue}
        className="rounded-full h-12 w-12 bg-yellow-400 flex items-center justify-center"
      >
        <div>
          <p>{coinDisplayValue(coin)}</p>
        </div>
      </div>
      <div className="flex flex-col items-center justify-center">
        <p>X</p>
        {editableAmounts && (
          <input
            type="number"
            value={coin.count}
            onChange={onEdit}
            className="block w-full py-3 px-0 mt-2 
                    text-gray-800 appearance-none 
                    border-b-2 border-gray-100
                    focus:text-gray-500 focus:outline-none focus:border-gray-200 mx-0 text-center"
          />
        )}
        {!editableAmounts && <p>{coin.count}</p>}
      </div>
    </div>
  );
}
