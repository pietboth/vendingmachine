import { ChangeCurrencyItem } from "./types";

export function calculateChange(totalAmount: number, amountPaid: number) {
  const changeRequired = amountPaid - totalAmount;

  return changeRequired;
}

export function getChangeInCoins(
  changeRequired: number,
  changeCurrency: ChangeCurrencyItem[]
) {
  let totalChange = 0;
  let changeItems: ChangeCurrencyItem[] = [];

  // order coin currency largets -> lowest
  // so we pick larger coins first
  const sortedCurrency = changeCurrency.sort((a, b) =>
    a.currencyValue > b.currencyValue ? -1 : 1
  );

  for (const changeCurrencyItem of sortedCurrency) {
    if (changeCurrencyItem.currencyValue === 0) {
      // this should never happen
      console.error(
        "Invalid currency item supplied, with a zero currency value",
        changeCurrencyItem
      );
    }

    const numberOfCoinToUse = Math.min(
      changeCurrencyItem.count,
      Math.trunc(
        (changeRequired - totalChange) / changeCurrencyItem.currencyValue
      )
    );

    const newAmount =
      totalChange + changeCurrencyItem.currencyValue * numberOfCoinToUse;

    if (newAmount <= changeRequired && numberOfCoinToUse > 0) {
      changeItems.push({
        ...changeCurrencyItem,
        count: numberOfCoinToUse,
      });
      totalChange = newAmount;
    }

    if (newAmount === changeRequired) {
      break;
    }
  }

  if (totalChange < changeRequired) {
    return {
      haveChange: false,
      totalChange: 0,
      changeItems: [],
    };
  }

  return {
    haveChange: true,
    totalChange,
    changeItems,
  };
}
