import { ChangeCurrencyItem } from "./types";
import cx from "classnames";
import ChangeItem from "./ChangeItem";

type Props = {
  coins: ChangeCurrencyItem[];
  editable?: boolean;
  onCoinAmountChanged?: (coin: ChangeCurrencyItem) => void;
};

export default function ChangeDisplay({
  coins,
  editable = false,
  onCoinAmountChanged,
}: Props) {
  return (
    <div
      className={cx(
        "grid md:grid-rows-1 grid-rows-2 auto-cols-min col-auto grid-flow-col justify-center md:justify-start",
        {
          "gap-5": editable,
          "gap-1": !editable,
        }
      )}
    >
      {coins.map((c) => (
        <ChangeItem
          key={c.currencyValue}
          coin={c}
          editableAmounts={editable}
          onCoinAmountChanged={onCoinAmountChanged}
        />
      ))}
    </div>
  );
}
