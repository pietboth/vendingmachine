import React, { useCallback, useEffect, useState } from "react";
import Modal from "../common/Modal";
import { useAppDispatch, useAppSelector } from "../hooks";
import ChangeDisplay from "./ChangeDisplay";
import formatDisplayPrice from "./formatDisplayPrice";
import ProductDisplay from "./ProductDisplay";
import { Product } from "./types";
import { confirmPurchaseInformation } from "./vendingMachineSlice";

export default function PurchaseConfirmationModal() {
  const dispatch = useAppDispatch();
  const [visible, setVisible] = useState(false);
  const changeDue = useAppSelector((state) => state.vendingMachine.changeDue);
  const product = useAppSelector(
    (state) => state.vendingMachine.purchasedProduct
  );

  useEffect(() => {
    if (changeDue) {
      setVisible(true);
    }
  }, [changeDue]);

  const dismiss = useCallback(() => {
    dispatch(confirmPurchaseInformation());
    setVisible(false);
  }, [dispatch]);

  if (!changeDue || !product) {
    return null;
  }

  return (
    <Modal
      visible={visible}
      onDismiss={dismiss}
      header="Item Purchased"
      confirmText="Thanks!"
    >
      <>
        <ProductDisplay product={product as Product} showRemaining={false} />
        <p className="mt-1">
          Your change due is: {formatDisplayPrice(changeDue.total)} in these
          coins:
        </p>
        <ChangeDisplay coins={changeDue.changeCurrency} />
      </>
    </Modal>
  );
}
