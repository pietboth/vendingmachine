import { Product } from "./types";
import cx from "classnames";
import ProductDisplay from "./ProductDisplay";

type Props = {
  product: Product;
  onSelect: (product: Product) => void;
  selected: boolean;
};

export default function ProductListItem({
  product,
  onSelect,
  selected,
}: Props) {
  const inStock = product.count > 0;

  return (
    <button
      onClick={() => onSelect(product)}
      className={cx("p-5 outline-none focus:outline-none rounded-md border-2", {
        "border-gray-100 bg-gray-100 cursor-pointer": !selected && inStock,
        "border-green-100 cursor-pointer": selected && inStock,
        "border-red-100 bg-red-100 cursor-not-allowed": !inStock,
      })}
    >
      <ProductDisplay product={product} showRemaining={true} />
    </button>
  );
}
