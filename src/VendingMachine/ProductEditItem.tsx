import React, { useCallback, useState } from "react";
import Button from "../common/Button";
import DeleteButton from "../common/DeleteButton";
import SymbolInput from "../common/SymbolInput";
import { Product } from "./types";

type Props = {
  product: Product;
  onChange: (product: Product) => void;
  onDelete?: (product: Product) => void;
  saveText?: string;
};
export default function ProductEditItem({
  product,
  onChange,
  onDelete = undefined,
  saveText = "Save",
}: Props) {
  const convertedCost = (product?.cost / 100).toString();
  // store form values as strings to enable clearing fields and showing
  // placeholders. conversion happens when initialising values and send to dispatch
  const [name, setName] = useState<string | undefined>(product.name);
  const [cost, setCost] = useState<string | undefined>(convertedCost);
  const [count, setCount] = useState<string | undefined>(
    product.count.toString()
  );

  const dirty =
    name !== product.name ||
    cost !== convertedCost ||
    count !== product.count.toString();

  const canSave =
    name !== undefined &&
    name.length > 0 &&
    cost !== undefined &&
    cost.length > 0 &&
    count !== undefined &&
    count.length > 0 &&
    dirty;

  const saveChanges = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      e.stopPropagation();

      const calculatedCost = cost ? Math.trunc(parseFloat(cost) * 100) : null;
      const calculatedCount = count ? parseInt(count) : null;

      if (calculatedCost === null || calculatedCount === null) {
        // this shouldn't happen due to canSave validation
        console.error(
          "calculated cost and count should be valid numbers",
          cost,
          count
        );
        return;
      }

      onChange({
        ...product,
        name: name as string,
        cost: calculatedCost,
        count: calculatedCount,
      });

      // clear the form if this is a create edit item
      if (product.id === 0) {
        setName(() => "");
        setCost(() => "");
        setCount(() => "");
      }
    },
    [onChange, product, cost, count, name]
  );

  return (
    <form onSubmit={saveChanges}>
      <div className="mb-1 flex mb-2 md:mb-0 md:items-center md:justify-center flex-col md:flex-row border-gray-200 border-b md:border-b-0 pb-2 md:pb-0">
        <input
          type="text"
          className="input mr-2"
          placeholder="Product name..."
          value={name as string}
          onChange={(e) => setName(e.target.value)}
        />
        <SymbolInput
          type="number"
          className="mr-2"
          step="0.01"
          placeholder="Cost (eg. 1.25)"
          min={0}
          value={cost as string}
          onChange={(e) => setCost(e.target.value)}
        />
        <SymbolInput
          symbol="#"
          type="number"
          placeholder="Product count"
          min={0}
          value={count as string}
          onChange={(e) => setCount(e.target.value)}
        />
        <div className="mt-2 ml-0 md:ml-5 md:mt-0 flex items-center justify-start">
          <Button type="submit" className="mr-1" disabled={!canSave}>
            {saveText}
          </Button>
          {onDelete && (
            <DeleteButton onClick={() => onDelete(product)}>
              Delete
            </DeleteButton>
          )}
        </div>
      </div>
    </form>
  );
}
