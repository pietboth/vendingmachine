import { useAppDispatch, useAppSelector } from "../hooks";
import ProductDisplay from "./ProductDisplay";
import React, { ChangeEvent, useCallback, useState } from "react";
import { buyProduct } from "./vendingMachineSlice";
import { Product } from "./types";
import Button from "../common/Button";
import SymbolInput from "../common/SymbolInput";

export default function Coinslot() {
  // store form value as string|undefined to allow clearing the input
  const [moneyInserted, setMoneyInserted] = useState<string>("");
  const [error, setError] = useState<string | null>(null);

  const selectedProduct = useAppSelector(
    (state) => state.vendingMachine.selectedProduct
  );

  const dispatch = useAppDispatch();

  const moneyValue = moneyInserted.length > 0 ? parseFloat(moneyInserted) : 0;

  const onMoneyInserted = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setMoneyInserted(e.target.value);
  }, []);

  const pay = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      e.stopPropagation();

      if (isNaN(moneyValue)) {
        setError("Please enter a valid decimal currency amount e.g 1.25");
        return;
      }

      dispatch(
        buyProduct({
          product: selectedProduct as Product,
          amountPaid: moneyValue * 100,
        })
      );
      setMoneyInserted("");
    },
    [dispatch, selectedProduct, moneyValue]
  );

  const canPay =
    selectedProduct &&
    !isNaN(moneyValue) &&
    moneyValue * 100 >= selectedProduct.cost;

  const title = canPay ? undefined : "Please insert more money to pay";

  return (
    <form onSubmit={pay}>
      <div className="container flex flex-col justify-start align-start text-left px-5">
        {!selectedProduct && (
          <p className="text-left">Please select an item.</p>
        )}
        {selectedProduct && (
          <>
            <ProductDisplay product={selectedProduct} showRemaining={false} />
            <p className="mt-5">Please enter money: </p>
            <div className="flex md:flex-row flex-col mt-2">
              <SymbolInput
                id="money"
                type="number"
                step="0.01"
                name="money"
                className="mr-2"
                onChange={onMoneyInserted}
                value={moneyInserted}
                required
              />
              <Button
                type="submit"
                className="mt-1"
                disabled={!canPay}
                title={title}
              >
                Pay
              </Button>
            </div>
            {error && <p className="text-pink-600 mt-1">{error}</p>}
          </>
        )}
      </div>
    </form>
  );
}
