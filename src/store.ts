import { configureStore } from "@reduxjs/toolkit";
import vendingMachineReducer from "./VendingMachine/vendingMachineSlice";
import authReducer from "./Authentication/authSlice";

const store = configureStore({
  reducer: {
    auth: authReducer,
    vendingMachine: vendingMachineReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;
