import React, { useCallback, useState } from "react";
import { Redirect } from "react-router";
import Button from "../common/Button";
import LoadingSpinner from "../common/LoadingSpinner";
import { useAppDispatch, useAppSelector } from "../hooks";
import { login } from "./authSlice";

export default function Login() {
  const authenticated = useAppSelector((state) => state.auth.authenticated);
  const loading = useAppSelector((state) => state.auth.loading);
  const error = useAppSelector((state) => state.auth.error);
  const [email, setEmail] = useState("test@test.com");
  const [password, setPassword] = useState("1234");
  const dispatch = useAppDispatch();

  const onLogin = useCallback(
    async (e: React.FormEvent) => {
      e.preventDefault();
      dispatch(login(email, password));
    },
    [dispatch, email, password]
  );

  if (authenticated) {
    return <Redirect to="/manage" />;
  }

  // grabbed the majority of the markup below off tailwind components to save some time
  return (
    <div className="flex flex-col h-screen">
      <div className="grid place-items-center mx-2 my-20 sm:my-auto">
        <div
          className="w-11/12 p-12 sm:w-8/12 md:w-6/12 lg:w-5/12 2xl:w-4/12 
            px-6 py-10 sm:px-10 sm:py-6 
            bg-white rounded-lg shadow-md lg:shadow-lg"
        >
          <h2 className="text-center font-semibold text-3xl lg:text-4xl text-gray-800">
            Login
          </h2>

          <form className="mt-10" method="POST" onSubmit={onLogin}>
            <label
              htmlFor="email"
              className="block text-xs font-semibold text-gray-600 uppercase"
            >
              E-mail
            </label>
            <input
              id="email"
              type="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              placeholder="e-mail address"
              autoComplete="email"
              className="block w-full py-3 px-1 mt-2 
                    text-gray-800 appearance-none 
                    border-b-2 border-gray-100
                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
              required
            />

            <label
              htmlFor="password"
              className="block mt-2 text-xs font-semibold text-gray-600 uppercase"
            >
              Password
            </label>
            <input
              id="password"
              type="password"
              name="password"
              placeholder="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              autoComplete="current-password"
              className="block w-full py-3 px-1 mt-2 mb-4
                    text-gray-800 appearance-none 
                    border-b-2 border-gray-100
                    focus:text-gray-500 focus:outline-none focus:border-gray-200"
              required
            />

            {loading && <LoadingSpinner />}
            {!loading && <Button type="submit">Login</Button>}
            {error && <p className="text-pink-600 mt-1">{error}</p>}
          </form>
        </div>
      </div>
    </div>
  );
}
