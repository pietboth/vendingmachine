import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AppDispatch } from "../store";
import { User } from "./types";

const testPassword = "1234";
const testUser = {
  email: "test@test.com",
  role: "admin",
};

interface AuthState {
  authenticated: boolean;
  loading: boolean;
  error: string | null;
  user?: User;
}

const initialState: AuthState = {
  authenticated: false,
  loading: false,
  error: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loggingIn: (state) => {
      state.loading = true;
    },
    userLoggedIn: (state, action: PayloadAction<User>) => {
      state.user = action.payload;
      state.authenticated = true;
      state.loading = false;
      state.error = null;
    },
    userLoginFailed: (state, action: PayloadAction<string>) => {
      state.user = undefined;
      state.authenticated = false;
      state.error = action.payload;
      state.loading = false;
    },
  },
});

export const { userLoggedIn, loggingIn, userLoginFailed } = authSlice.actions;

export const login = (email: string, password: string) => async (
  dispatch: AppDispatch
) => {
  dispatch(loggingIn());
  // pretend we're doing an ajax request here
  if (email !== testUser.email || password !== testPassword) {
    dispatch(userLoginFailed("Invalid login credentials supplied"));
    return;
  }

  setTimeout(() => {
    dispatch(userLoggedIn(testUser));
  }, 1000);
};

export default authSlice.reducer;
