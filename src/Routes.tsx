import React from "react";
import { Switch, Route } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

const Login = React.lazy(() => import("./Authentication/Login"));

const VendingMachine = React.lazy(
  () => import("./VendingMachine/VendingMachine")
);

const ManageMachine = React.lazy(
  () => import("./VendingMachine/ManageMachine")
);

export default function Routes() {
  return (
    <Switch>
      <Route exact path="/" component={VendingMachine} />
      <Route path="/login" component={Login} />
      <PrivateRoute path="/manage">
        <ManageMachine />
      </PrivateRoute>
      <Route path="/" component={VendingMachine} />
    </Switch>
  );
}
