import React, { Suspense } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import Routes from "./Routes";
import Navbar from "./common/Navbar";
import PageLoading from "./common/PageLoading";

function App() {
  return (
    <div className="App">
      <Suspense fallback={<PageLoading />}>
        <Router>
          <Navbar />
          <main className="container py-5 mx-auto px-4">
            <Routes />
          </main>
        </Router>
      </Suspense>
    </div>
  );
}

export default App;
